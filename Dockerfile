FROM ubuntu:14.04

env DEBIAN_FRONTEND noninteractive
RUN apt-get --yes update && apt-get --yes install software-properties-common
RUN add-apt-repository ppa:schooltool-owners/2.8
RUN apt-get --yes update && apt-get --yes upgrade && apt-get --yes install schooltool python-schooltool.virginia

USER schooltool

VOLUME ["/etc/schooltool/standard"]

CMD ["/usr/bin/supervisord", "--nodaemon", "-c", "/etc/schooltool/standard/supervisord.conf"]
