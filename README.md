# SchoolTool

This is [schooltool](https://code.launchpad.net/schooltool/) running in docker
containers

This is basically replicating
[this](http://www.cteresource.org/CanDo/phase2install.html) right now but the
hope is to abstract away supervisord and replace it with docker-compose

To get started, either run `make dev_run` or `make run`, depending on whether
you want files installed to system locations (eg `/etc/schooltool/standard`) or
just locally

## Dependencies

Requires docker & docker-compose
