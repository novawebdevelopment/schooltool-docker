# Migrating an existing schooltool instance

To migrate an existing schooltool instance, back up your apache certificates,
keys, and configs (configuration found in `/etc/apache2/sites-available` under
the name `cando.conf`). Back up `Data.fs` file and `blobs` directory found in
`/var/lib/schooltool` and the schooltool configurations
(`/etc/schooltool/standard`)


- `/etc/apache2/sites-available/cando.conf`
- SSL key (found in `/etc/ssl/private/`)
- SSL certificate (found in `/etc/ssl/certs/`)
- `/etc/schooltool/standard`
- `/var/lib/schooltool/Data.fs`
- `/var/lib/schooltool/blobs`

Once all of these files are in a safe location, set up the server as outlined
in [SETUP.md](SETUP.md).

On the new server, move all the files to the instructed locations

- SSL certificate -> `/etc/ssl/certs/schooltool.crt`
- SSL key -> `/etc/ssl/private/schooltool.key`
- `Data.fs` -> `/var/lib/schooltool/Data.fs`
- `blobs` -> `/var/lib/schooltool/blobs`
- Schooltool configuration directory (`standard`) -> `/etc/schooltool/standard`

After copying the files, run `sudo make migrate`, and then run `sudo make run`
to start the schooltool server
