There are a few files needed to be mounted as volumes:

* `$CONFIG_PATH`, defaults to /etc/schooltool/standard
	- Expects files such as schooltool.conf, zeo.conf, redis.conf

* `$DATABASE_PATH`, defaults to /var/lib/schooltool
	- Must be readable by the schooltool user


TODO: where to put ssl certificates and apache conf?
