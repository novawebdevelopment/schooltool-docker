Starting with an Ubuntu 18.04 LTS system

2) Install Docker-CE
	a) Install packages to allow `apt` to use a repository over HTTPS by entering the following text in the CLI:
	: ``` sudo apt-get install apt-transport-https ca-certificates curl
	software-properties-common ```
	b) Add Docker's official GPG key by entering the following text in the CLI:
	: ```curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key
	add```
	c) Use the following command to setup the **stable** repository by entering the following text in the CLI:
	: ```sudo add-apt-repository "deb [arch=amd64]
	https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"```
	d) Update the apt package index by entering the following text in the CLI:.
	: ```sudo apt-get update```
	e) Install the latest version of Docker CE by entering the following text in the CLI:
	: ```sudo apt-get install docker-ce```
	f) Verify that Docker CE is installed correctly by running the hello-world image by entering the following text in the CLI:.
	: ```sudo docker run hello-world```

3) Install docker-compose ([instructions here](https://docs.docker.com/compose/install/))
	a) Run this command to download the latest version of Docker Compose by entering the following text in the CLI:
	: `sudo curl -L
	"https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname
	-s)-$(uname -m)" -o /usr/local/bin/docker-compose`
	b) Apply executable permissions to the binary by entering the following text in the CLI:
	: `sudo chmod +x /usr/local/bin/docker-compose`
	c) Test the installation by entering the following text in the CLI:
	: `docker-compose –version`

3) Install schooltool-docker
	a) Download the [tar archive of this repository](https://gitlab.com/novawebdevelopment/schooltool-docker/-/archive/master/schooltool-docker-master.tar.gz)
	: `curl https://gitlab.com/novawebdevelopment/schooltool-docker/-/archive/master/schooltool-docker-master.tar.gz | tar -xz`
	b) Run `sudo make install` in the extracted directory
	: `cd schooltool-docker-master && sudo make install`
	c) Copy certificate and key to the /etc/ssl/certs/schooltool.crt and /etc/ssl/private/schooltool.key files respectively 
		- If you do not have certificates, run `sudo make certs`, make sure common name is the domain name of your website!
		: `sudo make certs`
	d) Modify `/etc/apache2/cando.conf` by typing in `nano /etc/apache2/cando.conf` and change hostnames and e-mail addresses 


8) If migrating, continue in [MIGRATE.md](MIGRATE.md), otherwise, run `sudo
make run` to start the schooltool server
