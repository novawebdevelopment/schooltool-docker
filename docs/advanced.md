By default, docker runs all the processes as the same user as the `schooltool`
user on the host machine, so that user is expected to exist in `/etc/passwd`.
This can be changed by changing the `$USER` and `$GROUP` environmental variables
before running docker-compose. Make sure that `$DATABASE_PATH` is owned and
writable by `$USER`
