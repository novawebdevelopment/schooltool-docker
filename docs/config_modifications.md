# Modifications made to configuration files to be properly used by docker-compose

## `redis.conf`

Remove `logfile` line

## `zeo.conf`

Remove `eventlog` tags and everything inside

## `schooltool.conf`

Change `zeoclient` `server` from `/var/run/schooltool/zeo.sock` to `zeo:8090`

Remove `error-log-file` line and `web-access-log-file` line



# Notes

It's important to note that in both `redis.conf` and `zeo.conf`, the respective
`bind` and `address` lines are overriden through command line arguments, so
they don't actually matter. In `zeo.conf`, the `address` line is required but
not read, but in `redis.conf` the `bind` line can, in fact, be deleted
