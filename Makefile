SRC_DIRECTORY=/usr/src/schooltool/

.PHONY: install
install: | /etc/schooltool/standard/ /var/lib/schooltool/ /etc/apache2/cando.conf /etc/ssl/certs/schooltool.crt /etc/ssl/private/schooltool.key $(SRC_DIRECTORY)/docker/ /etc/systemd/system/schooltool.service
	docker-compose --project-directory $(SRC_DIRECTORY)/docker/ pull
	systemctl enable schooltool

.PHONY: run
run:
	systemctl start schooltool

/etc/schooltool/standard/: standard/ | /etc/schooltool/
	cp -r $^ $@
	chown -R schooltool:schooltool $@

.PHONY: schooltool_user
schooltool_user: /var/lib/schooltool/
/var/lib/schooltool/:
	useradd schooltool --home-dir=/var/lib/schooltool/ --uid 1001 --create-home

/etc/apache2/cando.conf: cando.conf | /etc/apache2/
	cp $^ $@

/etc/schooltool/: | schooltool_user
	mkdir -p $@
	chown schooltool:schooltool $@

/etc/apache2/ /etc/ssl/certs/ /etc/ssl/private/:
	mkdir -p $@

/etc/ssl/certs/schooltool.crt /etc/ssl/private/schooltool.key: | /etc/ssl/certs/ /etc/ssl/private/
	echo "This is just a placeholder" > $@

$(SRC_DIRECTORY)/docker/: docker-compose.yml .env Dockerfile apache_proxy/ zodb/
	mkdir -p $@
	cp -r $^ $@

/etc/systemd/system/schooltool.service: schooltool.service
	cp $^ $@



.PHONY: migrate
migrate: /etc/schooltool/standard/ /var/lib/schooltool | schooltool_user
	find $< -type f -print0 | xargs --null \
		sed -i -e 's|127\.0\.0\.1|0.0.0.0|g' \
	               -e 's|/var/run/schooltool/zeo\.sock|zeo:8090|g' \
	               -e 's|/var/log/schooltool/.*|/dev/stdout|g'
	chown schooltool:schooltool $^


.PHONY: dev_run
dev_run: | $(PWD)/database $(PWD)/standard server.key server.crt
	DATABASE_PATH=$(PWD)/database CONFIG_PATH=$(PWD)/standard UGID=$(shell id -u):$(shell id -g) docker-compose up

.PHONY: certs
certs:
	sudo openssl req -x509 -newkey rsa:4096 -sha256 -nodes -keyout /etc/ssl/private/schooltool.key -out /etc/ssl/certs/schooltool.crt -days 3650

server.key server.crt:
	openssl req -x509 -newkey rsa:4096 -sha256 -nodes -keyout server.key -out server.crt -subj "/CN=localhost" -days 3650

$(PWD)/database:
	mkdir -p $@
